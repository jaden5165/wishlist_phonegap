angular.module('starter.controllers', [])

.controller('HomeCtrl', ['$scope', '$cordovaFacebook', '$state', '$ionicHistory', '$window', 'Auth', 'SessionData', function($scope, $cordovaFacebook, $state, $ionicHistory, $window, Auth, SessionData){

	$scope.login = {};
	$scope.signin = function(){
		var username = $scope.login.username;
		var password = $scope.login.password;

		console.log(username, password);

		Auth.$authWithPassword({
        	email: username,
        	password: password
    	})
    	.then(function(user) {
        	//Success callback
        	console.log('Authentication successful');
        	SessionData.setUser(username);
        	$state.go('userHome');
        }, function(error) {
        	//Failure callback
        	console.log('Authentication failure');
        });
	};

	$scope.showSignUp = function(){
    	$state.go('signUp');
	};

//     $scope.loginFacebook = function(){
//         console.log("here.");
//         console.log(ionic.Platform.platform());

//       if(!ionic.Platform.isWebView())
//       {
//         console.log("here1");
//         $cordovaFacebook.login(["public_profile", "email"]).then(function(success){
//             console.log(success);
//             Auth.$authWithOAuthToken("facebook", success.authResponse.accessToken, function(error, authData) {
//             if (error) {
//               console.log('Firebase login failed!', error);
//             } else {
//               console.log('Authenticated successfully with payload:', authData);
//             }
//           });
//         }, function(error){
//           console.log(error);
//         });        
//       }
//   else {
//         console.log("here2");
//         Auth.$authWithOAuthPopup("facebook", function(error, authData) {
//           if (error) {
//             console.log("Login Failed!", error);
//           } else {
//             console.log("Authenticated successfully with payload:", authData);
//           }
//         });
//         console.log("gg");
//        }
// };

$scope.loginFacebook = function(){
   
    var ref = new Firebase("https://ionic-firebase-test1.firebaseio.com/");


    if(ionic.Platform.isWebView()){

      $cordovaFacebook.login(["public_profile", "email"]).then(function(success){
     
        console.log(success);

        // ref.authWithOAuthToken("facebook", success.authResponse.accessToken, function(error, authData) {
        //   if (error) {
        //     console.log('Firebase login failed!', error);
        //   } else {
        //     console.log('Authenticated successfully with payload1:', authData);
        //     $state.go('userHome');
        //   }
        // });
          Auth.$authWithOAuthToken("facebook", success.authResponse.accessToken).then(function(authData) {
            console.log("Logged in as:", authData.uid);
            $state.go('userHome');
          }).catch(function(error) {
            console.error("Authentication failed:", error);
          });
     
      }, function(error){
        console.log(error);
      });        

    }
    else {
      // ref.authWithOAuthPopup("facebook", function(error, authData) {
      //   if (error) {
      //     console.log("Login Failed!", error);
      //   } else {
      //     console.log("Authenticated successfully with payload:", authData);
      //   }
      // });
         Auth.$authWithOAuthPopup("facebook").then(function(authData) {
            console.log("Logged in as:", authData.facebook.displayName);
            SessionData.setUser(authData.facebook.displayName);
            $state.go('userHome');
          }).catch(function(error) {
            console.log("Authentication failed:", error);
          });
    }

  };

}])

.controller('UserHomeCtrl', ['$scope', '$state', '$ionicHistory', 'Auth', 'WishFb', function($scope, $state, $ionicHistory, Auth, WishFb){

	$scope.wishes = WishFb;
	$scope.logout = function(){
		Auth.$unauth();
		$state.go('home');
	};

	$scope.showAddWish = function(){
   	$state.go('addWish');
	};
	
}])

.controller('SignUpCtrl', ['$scope', '$state', 'Auth', function($scope, $state, Auth){
	
	$scope.login = {};

	$scope.signup = function(){
		var username = $scope.login.username;
		var password = $scope.login.password;

		console.log(username, password);

		Auth.$createUser({
        	email: username,
        	password: password
    	})
    	.then(function(user) {
        	//Success callback
        	console.log('Sign Up successful');
        	$state.go('userHome');
        }, function(error) {
        	//Failure callback
        	console.log('Sign Up failure');
        });
	};

	$scope.showSignIn = function(){
		$state.go('home');
	};


}])

.controller('AddWishCtrl', ['$scope','$state', '$firebase', 'WishFb', 'SessionData', function($scope,$state, $firebase, WishFb, SessionData){

	$scope.user = {};
	$scope.wishes = WishFb;

    $scope.showUserHome = function(){
       $state.go('userHome');
    };

    $scope.add = function(){
        var user = SessionData.getUser();

        $scope.wishes.$add({
            wish: $scope.user.wish,
            email: user
        }).then(function(ref) {
            console.log(ref);
           $state.go('userHome');
        }, function(error) {
            console.log("Error:", error);
        });
    };
}]);