angular.module('starter.services', [])

.factory('Auth', function($firebaseAuth){
	var usersRef = new Firebase("https://ionic-firebase-test1.firebaseio.com/");
  	return $firebaseAuth(usersRef);
})

.factory('WishFb', function($firebaseArray){
	var wishRef = new Firebase("https://ionic-firebase-test1.firebaseio.com/MyWish/");
  	return $firebaseArray(wishRef);
})

// .factory('SimpleAuth', function($firebaseSimpleLogin){
// 	var simRef = new Firebase("https://ionic-firebase-test1.firebaseio.com/MyWish/");
//   	return $firebaseSimpleLogin(simRef);
// })

.factory('SessionData', function() {
    var user = '';

    return {
        getUser: function() {
            return user;
        },
        setUser: function(value) {
            user = value;
        }
    };
});
